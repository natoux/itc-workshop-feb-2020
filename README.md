# Datagen x ITC workshop on Autoencoders - February 2020

## Preparing for the workshop
  
Please prepare by setting up an environment as follows:  

1) `cd /<your-projects-folder>`  
2) git clone this repository  
3) `cd <your_path>/itc-workshop-feb-2020`  
5) `pip install virtualenv`
4) `virtualenv -p python3 <your_path>/datagen-workshop`  
5) `source <your_path>/datagen-workshop/bin/activate`  
6) `pip install -r requirements.txt`  
7) `jupyter notebook`

This installation was tested under Python 3.7.4
If you have both Python 2 and 3 on your computer, consider redoing the installation steps using `pip3` instead of `pip` if you encounter problems.

If you get an error, try installing the package independently and without a version.   
Example: pip install numpy.  

## Structure

Using Keras, we will:  
1) Build, train and use an Autoencoder for MNIST  
2) Extend to Variational Autoencoder (VAE) and Conditional Autoencoder (CAE)  
3) Generate real-world images (faces)  
  
  
![picture](animation_smiling.gif)
